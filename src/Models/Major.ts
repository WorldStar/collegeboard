﻿namespace CollegeBoard.Models {

    export class Major {
        public item: Object;
        public key: string;

        constructor(id: string, name:string, view:string) {
            this.item = {id:id, name:name, view:view};
        }
    }
}
