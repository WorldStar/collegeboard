﻿namespace CollegeBoard.Models {

    export class Global {
        public static school: string;
        public static major: string;
        public static date: string;
        public static course: string;

    }
}
