﻿namespace CollegeBoard.Controllers {

    export class ProfileController extends BaseController<ViewModels.ProfileViewModel> {

        //#region Injection

        public static ID = "ProfileController";

        public static get $inject(): string[] {
            return [
                "$scope",
                Services.Utilities.ID,
                Services.Configuration.ID
            ];
        }

        constructor(
            $scope: ng.IScope,
            private Utilities: Services.Utilities,
            private Configuration: Services.Configuration) {
            super($scope, ViewModels.ProfileViewModel);
        }

        //#endregion

        //#region BaseController Overrides

        protected view_beforeEnter(event?: ng.IAngularEvent, eventArgs?: Ionic.IViewEventArguments): void {
            super.view_beforeEnter(event, eventArgs);
            var userinfo = JSON.parse(localStorage.getItem('profile'));

            this.viewModel.clientID = userinfo.clientID;

            if(userinfo.nickname != null)
                this.viewModel.nickname = userinfo.nickname;
            else
                this.viewModel.nickname = "";

            if(userinfo.updated_at != null)
                this.viewModel.updated_at = userinfo.updated_at;
            else
                this.viewModel.updated_at = "";

            if(userinfo.email != null)
                this.viewModel.useremail = userinfo.email;
            else
                this.viewModel.useremail = "";

            if(userinfo.user_id != null)
                this.viewModel.userID = userinfo.user_id;
            else
                this.viewModel.userID = "";

            if(userinfo.name != null)
                this.viewModel.username = userinfo.name;
            else
                this.viewModel.username = "";

            if(userinfo.picture != null)
                this.viewModel.userphoto = userinfo.picture;
            else
                this.viewModel.userphoto = "";
        }

        //#endregion
    }
}
