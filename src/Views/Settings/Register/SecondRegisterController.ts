﻿namespace CollegeBoard.Controllers {

    export class SecondRegisterController extends BaseController<ViewModels.SecondRegisterViewModel> {

        //#region Injection

        public static ID = "SecondRegisterController";

        public static get $inject(): string[] {
            return [
                "$scope",
                "$ionicPopup",
                Services.Utilities.ID,
                Services.Configuration.ID
            ];
        }

        constructor(
            $scope: ng.IScope,
            private $ionicPopup: any,
            private Utilities: Services.Utilities,
            private Configuration: Services.Configuration) {
            super($scope, ViewModels.SecondRegisterViewModel);
        }

        //#endregion

        //#region BaseController Overrides

        protected view_beforeEnter(event?: ng.IAngularEvent, eventArgs?: Ionic.IViewEventArguments): void {
            super.view_beforeEnter(event, eventArgs);

            var courselist = ["course 1","course 2","course 3", "course 4", "course 5", "course 6", "course 7","course 8"];

            /*var courselist = [
                new Models.School("course 1", "course 1"),
                new Models.School("course 2", "course 2"),
                new Models.School("course 3", "course 3"),
                new Models.School("course 4", "course 4"),
                new Models.School("course 5", "course 5"),
                new Models.School("course 6", "course 6"),
                new Models.School("course 7", "course 7"),
                new Models.School("course 8", "course 8")
            ];*/
            var selectedcourselist = [];
            //var courselist = [
            //    new Models.School("course 1", "u0"),
            //    new Models.School("course 2", "u1"),
            //    new Models.School("course 3", "u2"),
            //    new Models.School("course 4", "u3")
            //];
            this.viewModel.courselist = courselist;
            this.viewModel.selectedcourselist = selectedcourselist;
        }
        protected next(){
            var courseval = '';
            var i = 0;
            for(var course1 in this.viewModel.selectedcourselist){
                if(i == 0){
                    courseval = this.viewModel.selectedcourselist[i];
                }else{
                    courseval = courseval + "," + this.viewModel.selectedcourselist[i];
                }
                i++;
            }
            if(courseval == ""){
                var alertPopup = this.$ionicPopup.alert({
                    title: 'CollegeBoard',
                    template: 'Please select the courses.'
                });
                return;
            }
            localStorage.setItem('course', courseval);
            location.hash = '#/app/register/third';
        }

        protected skip(){
            localStorage.setItem('course', "");
            location.hash = '#/app/register/third';
        }
        public removeCourse(course){
        }
        protected onItemDelete(course){
            console.log(course);
            this.viewModel.selectedcourselist.splice(
                this.viewModel.selectedcourselist.indexOf( course ), 1 );
        }
        protected autoItemClicked(callback){
            console.log(callback.item);
            this.viewModel.selectedcourselist.push(callback.item);
            console.log(this.viewModel.selectedcourselist);
        }
        protected itemsRemoved(callback){
            console.log(callback);
        }
        //#endregion
    }
}
