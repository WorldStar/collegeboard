﻿namespace CollegeBoard.Controllers {

    export class ThirdRegisterController extends BaseController<ViewModels.ThirdRegisterViewModel> {

        //#region Injection

        public static ID = "ThirdRegisterController";

        public static get $inject(): string[] {
            return [
                "$scope",
                "$ionicPopup",
                Services.Utilities.ID,
                Services.Configuration.ID
            ];
        }

        constructor(
            $scope: ng.IScope,
            private $ionicPopup: any,
            private Utilities: Services.Utilities,
            private Configuration: Services.Configuration) {
            super($scope, ViewModels.ThirdRegisterViewModel);
        }

        //#endregion

        //#region BaseController Overrides

        protected view_beforeEnter(event?: ng.IAngularEvent, eventArgs?: Ionic.IViewEventArguments): void {
            super.view_beforeEnter(event, eventArgs);
            this.viewModel.school = localStorage.getItem("school");
            this.viewModel.major = localStorage.getItem("major");
            this.viewModel.graduateddate = localStorage.getItem("graduateddate");
            this.viewModel.course = localStorage.getItem("course");
            this.viewModel.schoolmail = "";
        }
        protected next(){
            var mail1 = this.viewModel.schoolmail.toString();

            if(mail1 == null || mail1 == ""){
                var alertPopup = this.$ionicPopup.alert({
                    title: 'CollegeBoard',
                    template: 'Please input the school verification mail.'
                });
                return;
            }
            localStorage.setItem('schoolemail', mail1);
            location.hash = '#/app/register/verify';
        }

        protected skip(){
            location.hash = '#/';
        }
        //#endregion
    }
}
