namespace CollegeBoard.ViewModels {

    export class ProfileViewModel {
        public userphoto: string;
        public useremail: string;
        public given_name: string;
        public family_name: string;
        public username: string;
        public nickname: string;
        public clientID: string;
        public updated_at: string;
        public userID: string;
    }
}
