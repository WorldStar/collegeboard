namespace CollegeBoard.Controllers {

    export class VerifyRegisterController extends BaseController<ViewModels.VerifyRegisterViewModel> {

        //#region Injection

        public static ID = "VerifyRegisterController";

        public static get $inject(): string[] {
            return [
                "$scope",
                "$ionicPopup",
                Services.Utilities.ID,
                Services.Configuration.ID
            ];
        }

        constructor(
            $scope: ng.IScope,
            private $ionicPopup: any,
            private Utilities: Services.Utilities,
            private Configuration: Services.Configuration) {
            super($scope, ViewModels.VerifyRegisterViewModel);
        }

        //#endregion

        //#region BaseController Overrides

        protected view_beforeEnter(event?: ng.IAngularEvent, eventArgs?: Ionic.IViewEventArguments): void {
            super.view_beforeEnter(event, eventArgs);
            this.viewModel.verifycode = "";

        }
        protected next(){
            var code = this.viewModel.verifycode.toString();
            if(code == null || code == ""){
                var alertPopup = this.$ionicPopup.alert({
                    title: 'CollegeBoard',
                    template: 'Please input verify code from your school mail adress.'
                });
                return;
            }
            localStorage.setItem('verifycode', code);
            location.hash = '#/';
        }

        protected skip(){
            location.hash = '#/';
        }
        //#endregion
    }
}
