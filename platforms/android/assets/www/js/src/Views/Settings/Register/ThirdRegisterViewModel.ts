namespace CollegeBoard.ViewModels {

    export class ThirdRegisterViewModel {
        public school: string;
        public major: string;
        public graduateddate: string;
        public course: string;
        public schoolmail: string;
    }
}
