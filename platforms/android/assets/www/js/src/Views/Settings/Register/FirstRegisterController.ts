namespace CollegeBoard.Controllers {

    export class FirstRegisterController extends BaseController<ViewModels.FirstRegisterViewModel> {

        //#region Injection

        public static ID = "FirstRegisterController";

        public static get $inject(): string[] {
            return [
                "$scope",
                "$ionicPopup",
                Services.Utilities.ID,
                Services.Configuration.ID
            ];
        }

        constructor(
            $scope: ng.IScope,
            private $ionicPopup: any,
            private Utilities: Services.Utilities,
            private Configuration: Services.Configuration) {
            super($scope, ViewModels.FirstRegisterViewModel);
        }

        //#endregion

        //#region BaseController Overrides

        protected view_beforeEnter(event?: ng.IAngularEvent, eventArgs?: Ionic.IViewEventArguments): void {
            super.view_beforeEnter(event, eventArgs);
            var schools = [
                new Models.School("US University", "US University"),
                new Models.School("Astrallia University", "Astrallia University"),
                new Models.School("Germen University", "Germen University"),
                new Models.School("French University", "French University")
            ];
            this.viewModel.schoollist = schools;
            var majorlist1 = ["IT 1","IT 2","IT 3","Marketing 1", "Marketing 2", "Marketing 3", "Marketing 4", "business"];
            //var majorlist = [
            //    new Models.Major("1", "IT 1", "IT 1"),
            //    new Models.Major("2", "IT 2", "IT 2"),
            //    new Models.Major("3", "IT 3", "IT 3"),
            //    new Models.Major("4", "Marketing 1", "Marketing 1"),
            //    new Models.Major("5", "Marketing 2", "Marketing 2"),
            //    new Models.Major("6", "Marketing 3", "Marketing 3"),
            //    new Models.Major("7", "Marketing 4", "Marketing 4"),
            //    new Models.Major("8", "business", "business"),
            //];
            var majorlist = [
                new Models.School("IT 1", "IT 1"),
                new Models.School("IT 2", "IT 2"),
                new Models.School("IT 3", "IT 3"),
                new Models.School("Marketing 1", "Marketing 1"),
                new Models.School("Marketing 2", "Marketing 2"),
                new Models.School("Marketing 3", "Marketing 3"),
                new Models.School("Marketing 4", "Marketing 4"),
                new Models.School("business", "business"),
            ];
            var selmajorlist = [];
            this.viewModel.selmajorlist = selmajorlist;
            this.viewModel.selschoollist = selmajorlist;

            this.viewModel.majorlist = majorlist;
            this.viewModel.majorlist1 = majorlist1;
            this.viewModel.graduateddate = "";
        }
        protected next(){
            var dd = this.viewModel.selschoollist.toString();
            var dd1 = this.viewModel.selmajorlist.toString();
            var date1 = this.viewModel.graduateddate;
            if(dd == null || dd == ""){
                var alertPopup = this.$ionicPopup.alert({
                    title: 'CollegeBoard',
                    template: 'Please select a school.'
                });
                return;
            }
            if(dd1 == null || dd1 == ""){
                var alertPopup = this.$ionicPopup.alert({
                    title: 'CollegeBoard',
                    template: 'Please select a major.'
                });
                return;
            }
            if(date1 == ""){
                var alertPopup = this.$ionicPopup.alert({
                    title: 'CollegeBoard',
                    template: 'Please select a graduated date.'
                });
                return;
            }
            localStorage.setItem('school', dd);
            localStorage.setItem('major', dd1);

            var date = new Date(date1);
            var day = date.getDate();
            var monthIndex = date.getMonth() + 1;
            var year = date.getFullYear();
            console.log(""+monthIndex+"/"+day+"/"+year);
            localStorage.setItem('graduateddate', ""+monthIndex+"/"+day+"/"+year);
            //localStorage.setItem('graduateddate', "2012/10/11");
            location.hash = '#/app/register/second';
        }
        protected clickedMethod(callback) {
            // print out the selected item
            console.log(callback.item);

            // print out the component id
            console.log(callback.componentId);

            // print out the selected items if the multiple select flag is set to true and multiple elements are selected
            console.log(callback.selectedItems);
        }
        protected changedDate(datetimeValue) {
            this.viewModel.graduateddate = datetimeValue;

            //Models.Gobal.date = 'asd';
        }
        //#endregion
    }
}
